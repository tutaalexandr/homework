import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    currency: '$',
    history: [

    ]
  },
  mutations: {
    addHistory(state, {amount, type, comment, date}) {

      state.history.push({
        amount,
        type,
        comment,
        date: date ? date : moment().format('YYYY.MM.DD HH:mm:ss')
      });

      localStorage.setItem('history', JSON.stringify(state.history));
    },

    changeCurrency(state, currency) {
      state.currency = currency;
    }
  },
  getters: {
    total: state => {
      return state.history.reduce((sum, row) => {
        return row.type === 'debit' ? sum + row.amount : sum - row.amount;
      }, 0).toFixed(2);
    },
  },

  actions: {
    fetchHistory({commit}) {

      const history = localStorage.getItem('history');

      if (history) {
        // BAD Practices
        // state.history = JSON.parse(history);

        const arrayOfHistory = JSON.parse(history);

        arrayOfHistory.forEach(item => commit('addHistory', item))
      }
    }
  }
});

import App from './App';

new Vue({
  el: '#app',
  store,
  mounted() {
    store.dispatch('fetchHistory');
  },
  render: h => h(App),
});